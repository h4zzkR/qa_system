import time
import sys
import os
from yaspin import yaspin
import nltk
import gensim
import torch
import numpy as np
from razdel import tokenize


class TokenEngine(object):
    def __init__(self, w2i, i2w, pad_idx, max_len):
        self.word2index = w2i
        self.pad_idx = pad_idx
        self.index2word = i2w
        self.max_len = max_len

    def code_word(self, word):
        try:
            return self.word2index[word]
        except Exception as e:
            return -1

    def code_sentense(self, sentense):
        # ['hello', 'world']
        sentense_ = []
        for w in sentense:
            stw = self.code_word(w.lower().replace('ё', 'е'))
            if stw != -1:
                sentense_.append(stw)
        return sentense_

    def tknz(self, sent):
        return [_.text for _ in list(tokenize(sent))]

    def pad_sentence(self, coded_sentence, mode='post'):
        # ['hello', 'world']
        if mode == 'post':
            r = self.max_len - len(coded_sentence)
            [coded_sentence.append(1) for _ in range(r)]
            assert len(coded_sentence) == self.max_len
            return coded_sentence

        elif mode == 'begin':
            pass

    def drop_grammar(self, sentence):
        stop_list = ',#$~^&'
        sent = []
        for token in sentence:
            if token not in stop_list:
                sent.append(token)
        return sent


    def prepare_sentence(self, sentence):
        #'hello world'
        sentence = self.tknz(sentence)
        sentence = self.drop_grammar(sentence)
        sentence = self.code_sentense(sentence)
        if len(sentence) != 0:
            sentence = self.pad_sentence(sentence)
            return sentence
        else:
            return -1


    def mask(self, coded):
        mask_ = [1 if i != 1 else 0 for i in coded]
        lengths = len(np.where(np.array(mask_) == 1)[0])
        return coded, mask_, lengths


    def prepare_sentences(self, sents):
        text_data = []
        masks = []
        lengths = []
        for s in sents:
            sentence = self.prepare_sentence(s)
            if sentence != -1:
                t, m, l = self.mask(sentence)
                text_data.append(t); masks.append(m); lengths.append(l)
        return (text_data, lengths, masks)
