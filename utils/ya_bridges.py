import os
import subprocess
import sys
from utils.functions import info, error, good_news
from YaDiskClient.YaDiskClient import YaDisk as YDisk
from yaspin import yaspin

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = ROOT_DIR[:ROOT_DIR.find('qa_system') + 10]
sys.path.insert(1, ROOT_DIR)

class YaDisk(object):
    def __init__(self, user=None, password=None):
        if user is None or password is None:
            user = input('$ YaDisk username: ')
            password = input('$ YaDisk password: ')
        try:
            self.disk = YDisk(user, password)
        except Exception as e:
            error(e)
        good_news(f'Succesfully logged in')

    def dl(self, link, p2dl, name):
        try:
            with yaspin(text=f'{name.title()} is downloading...', color='red') as sp:
                self.disk.download(link, p2dl)
                sp.ok("✅ ")
        except Exception as e:
            sp.fail("💥 ")
            error(e)
            return 1
        good_news(f'{name.title()} downloaded')

    def up(self, path, up_to, name):
        try:
            with yaspin(text=f'{name.title()} is uploading...', color='red') as sp:
                self.disk.upload(path, up_to)
                sp.ok("✅ ")
        except Exception as e:
            sp.fail("💥 ")
            error(e)
            return 1
        good_news(f'{name.title()} uploaded')


def init():
    # user = input('$ YaDisk username: ')
    # password = input('$ YaDisk password: ')
    disk = YaDisk()
    disk.dl('flyme_hennessy_ry09iu_6.7.12.29-Beta.zip', '/home/arc4num/a.zip', 'File')

def dl(link, path, name='File'):
    YaDisk().dl(link, path, name)

def up(link, path, name='File'):
    YaDisk().up(link, path, name)


if __name__ == "__main__":
    # state = rmega_dl('https://mega.nz/#!zyJgRKKB!lLNhhgTk0wXEaQVXNJKXoHoGFNv95VEdvn77hfHWZps', '~/home.zip', 'dfd')
    # init()
    dl('Downloads/409.msi', '/home/arc4num/409.msi', 'File')