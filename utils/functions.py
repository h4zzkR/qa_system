import time
import sys
import os
from yaspin import yaspin
from termcolor import colored


def reporthook(count, block_size, total_size):
    global start_time
    if count == 0:
        start_time = time.time()
        return
    duration = time.time() - start_time
    progress_size = int(count * block_size)
    speed = int(progress_size / (1024 * duration))
    percent = min(int(count*block_size*100/total_size),100)
    downloaded = progress_size / (1024 * 1024)
    total_size_ = total_size / 1024 / 1024

    done = int(50 * int(downloaded) / total_size_)
    wait_time = round(total_size_ * 1024 / speed / 3600, 1)
    sys.stdout.write('\r[ {}{} | {}% | {}MB | {}KB/s | {}sec | ETA: {}hs ]'.format('█' * done, '.' * (50-done), percent,
                                                                         round(downloaded, 1), speed, int(duration), wait_time))
    sys.stdout.flush()

def out(msg):
    print(colored('<', 'red'), str(msg), sep=' ')

def info(msg):
    mode = f"""[{colored('I', 'green')}]"""
    print(f'{mode} {msg}')

def warning(msg):
    mode = f"""[{colored('W', 'magenta')}]"""
    print(f'{mode} {msg}')

def good_news(msg):
    mode = f"""[{colored('✓', 'green')}]"""
    print(f'{mode} {msg}')

def error(msg):
    mode = f"""[{colored('E', 'red')}]"""
    print(f'{mode} {msg}')
