# -*- coding: utf-8 -*-
"""
Сжимаем модель w2v, оставляем только используемые в тренировочном датасете слова.
Результат сохраняем в data
"""


import os.path
import sys
import gensim
import io

from yaspin import yaspin

#TODO Используй автожнкодеры и потом сравнивай вектора для модели синонимичсности
from utils.functions import info, error, warning, good_news

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = ROOT_DIR[:ROOT_DIR.find('qa_system') + 10]
sys.path.insert(1, ROOT_DIR)


def decrease(w2v_path = 'data/embed.module/k_vectors/W2V.CBOW=1_WIN=5_DIM=64.bin'):
    relative_w2v = w2v_path
    w2v_path = os.path.join(ROOT_DIR, w2v_path)
    dataset = os.path.join(ROOT_DIR, 'data/known_words.txt')
    info(u'Loading w2v from {}'.format(w2v_path))
    w2v = gensim.models.KeyedVectors.load_word2vec_format(w2v_path, binary=True)
    w2v_dims = len(w2v.syn0[0])

    all_words = set()
    with io.open(dataset, 'r', encoding='utf-8') as rdr:
        for line in rdr:
            word = line.strip()
            if word in w2v:
                all_words.add(word)

    nb_words = len(all_words)
    info('{} total words in minimized vocab'.format(nb_words))


    info('Writing w2v text model...')
    with io.open(os.path.join(ROOT_DIR, 'data/w2v_distill.tmp'), 'w', encoding='utf-8') as wrt:
        wrt.write(u'{} {}\n'.format(nb_words, w2v_dims))

        for word in all_words:
            word_vect = w2v[word]
            wrt.write(u'{} {}\n'.format(word, u' '.join([str(x) for x in word_vect])))

    del w2v
    w2v = gensim.models.KeyedVectors.load_word2vec_format(os.path.join(ROOT_DIR, 'data/w2v_distill.tmp'),
                                                          binary=False)

    info('Storing binary w2v model...')
    name = relative_w2v[relative_w2v.find('k_vectors/')+10:]
    out_dir = os.path.join(ROOT_DIR, f'data/embed.module/k_vectors/{"MIN." + name}')
    w2v.wv.save_word2vec_format(out_dir, binary=True)
    os.system(f'!rm -rf {0}'.format(os.path.join(ROOT_DIR, 'data/w2v_distill.tmp')))
    good_news('Vectors minimized')
