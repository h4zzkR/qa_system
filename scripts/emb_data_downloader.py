#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os.path
import sys
import urllib.request
import json
from multiprocessing.dummy import Pool as ThreadPool

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = ROOT_DIR[:ROOT_DIR.find('qa_system') + 10]
sys.path.insert(1, ROOT_DIR)

from utils.functions import reporthook, info
from modules.embedding_module.embed_data_preprocessor import main as preprocessor

DWND_PATH = os.path.join(ROOT_DIR, "data/embed.module/embed.data_urls.json")


def download(data):
    url, size, name, path = data
    print(info(f'Downloading {name} of size {size}...'))
    urllib.request.urlretrieve(url, path, reporthook)
    print(info(f'[✓] {name.title()} downloaded'))

def downloader(links_vocab):
    pool = ThreadPool(4)
    urls = [links_vocab[key]['url'] for key in links_vocab.keys()]
    sizes = [links_vocab[key]['size'] for key in links_vocab.keys()]
    names = [links_vocab[key]['name'] for key in links_vocab.keys()]
    pathes = [os.path.join(ROOT_DIR, f"data/embed.module/corpuses/{name}") for name in names]
    data = list(zip(urls, sizes, names, pathes))
    pool.map(download, data)
    pool.close()
    pool.join()

def main():
    with open(DWND_PATH, 'r') as f:
        links = json.load(f)
        downloader(links)
        preprocessor(DWND_PATH)

if __name__ == "__main__":
    main()