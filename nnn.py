import sys
import os
import time


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = ROOT_DIR[:ROOT_DIR.find('qa_system') + 10]
sys.path.insert(1, ROOT_DIR)
from utils.functions import info, error, warning, good_news, out

import importlib
from modules.embedding_module.embed_main import build_embedding_layer
from modules.intents_detector_module.intents_data_preprocessor import main as intents_preproc
from scripts.emb_data_downloader import main as emb_corpus_downloader
from scripts.w2v_decrease import decrease
from terminal_modules import Cooker, Loader, Runner
from termcolor import colored
import torch
import numpy as np
import re
import code
import inspect

from cmd import Cmd

def reload():
    importlib.reload(build_embedding_layer)
    importlib.reload(intents_preproc)
    importlib.reload(emb_corpus_downloader)
    importlib.reload(decrease)
    importlib.reload(Cooker)
    importlib.reload(Loader)
    importlib.reload(Runner)

def arg2type(arg):
    if arg.isdigit():
        arg = float(arg)
    elif arg == 'True' or arg == 'true':
        arg = True
    elif arg == 'False' or arg == 'false':
        arg = False
    return arg

def arg_parser(command):
    command = command.strip()
    args = command.split('--')
    args_dict = {}
    for i in args[1:]:
        arg, val = i.split()
        args_dict.update({arg : arg2type(val)})
    return args_dict

class MyPrompt(Cmd):
    prompt = colored('> ', 'green')
    intro = "System CLI. '?' or 'help' for commands list. CtrlD or 'exit' for exit."

    def preloop(self):
        USE_CUDA = torch.cuda.is_available()
        self.device = torch.device("cuda" if USE_CUDA else "cpu")
        self.GLOBAL_VARS = {
            'device': self.device
        }
        self.m = {'loader' : Loader(self.GLOBAL_VARS),
                  'runner' : Runner(self.GLOBAL_VARS),
                  'cooker' : Cooker(self.GLOBAL_VARS),
                  } #modules
        self.history = []
        self.preloader()

    def preloader(self):
        self.mode = input(f'{colored(">", "green")} Mode (dev, inf): ')
        if self.mode != 'dev' and self.mode != 'inf': raise NotImplementedError
        if self.mode == 'dev':
            info('Loading embeddings...')
            self.update_vars(self.m['loader'].parse('emb', arg_parser('--mode min')))

    def sync_modules(self):
        for k in self.m.keys():
            self.m[k].vars = self.GLOBAL_VARS

    def update_vars(self, dict_ob=None):
        if dict_ob == None:
            return 0
        [self.GLOBAL_VARS.update({key : dict_ob[key]}) for key in dict_ob.keys()]
        info(f'{" + ".join([key for key in dict_ob.keys()])} loaded')

    def do_load_embeddings(self, args):
        self.help_load_embeddings() if 'help' in args else self.update_vars(self.m['loader'].parse('emb', arg_parser(args)))

    def postcmd(self, stop, line):
        if stop == True or 'exit' in line:
            sys.exit()
        elif 'cook_embeddings' in line:
            self.history.append(('embeddings corpuses downloaded, processed and ready to training', arg_parser(line)))
        elif 'load_embeddings' in line:
            self.history.append(('embeddings loaded or initialized', arg_parser(line)))
        elif 'cook_intents' in line:
            self.history.append(('intents loaded, processed and ready to training', arg_parser(line)))

    def do_reload(self, args):
        reload()

    def do_history(self, args):
        for i in self.history:
            message = i[0][0].upper() + i[0][1:] + ' | '
            args = []
            for j in i[1].keys():
                args.append(f'{j} : {i[1][j]}')
            message += '* {' + ' '.join(args) + '}'
            out(message)

    def do_cook_embeddings(self, args):
        self.update_vars(self.m['cooker'].parse('emb', arg_parser(args)))

    def do_cook_intents(self, args):
        self.update_vars(self.m['cooker'].parse('intents', arg_parser(args)))

    def do_run_smaller_embeddings(self, args):
        self.update_vars(self.m['runner'].parse('emb_decrease', arg_parser(args)))

    def do_run_smaller_emb(self, args):
        self.do_run_smaller_embeddings(args)

    def help_run_smaller_embeddings(self):
        pass
    #   --w2v_path

    def help_cook_intents(self):
        pass
    #   --batch_size --shuffle --drop_last

    def show_var(self, command):
        flag = 0
        try:
            for k in self.GLOBAL_VARS.keys():
                if k in command:
                    to_comm = command[:command.find(k)] + f"self.GLOBAL_VARS['{k}']" + command[command.find(k) + len(k):]
                    out(eval(to_comm))
                    flag = 1
                if flag == 1:
                    break
        except Exception as e:
            error(e)
        if flag == 0:
            if 'history' in command:
                out(self.history)

    def object_info(self, object):
        if isinstance(object, np.ndarray):
            pass

    def do_d(self, attr):
        self.show_var(attr)

    def help_load_embeddings(self):
        print('HELP')
        pass
    # emb: --mode --freeze -save_gensim

    def do_exit(self, inp):
        out('Interrupting...')
        return True

    def do_clear(self, arg):
        try:
            os.system('clear')
        except Exception as e:
            error(e)

    def do_vars(self, arg):
        out(' '.join(list(self.GLOBAL_VARS.keys())))

    def default(self, inp):
        if inp == 'x' or inp == 'q':
            return self.do_exit(inp)

        else:
            self.show_var(inp)

    do_EOF = do_exit

def main():
    MyPrompt().cmdloop()


if __name__ == '__main__':
    main()