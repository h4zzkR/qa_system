import sys
import os
import time
import shlex
import argparse


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = ROOT_DIR[:ROOT_DIR.find('qa_system') + 10]
sys.path.insert(1, ROOT_DIR)
from utils.functions import info, error, warning, good_news

from modules.embedding_module.embed_main import build_embedding_layer
from modules.intents_detector_module.intents_data_preprocessor import main as intents_preproc
from scripts.emb_data_downloader import main as emb_corpus_downloader
from scripts.w2v_decrease import decrease
from termcolor import colored
import torch


def parse_args(command, args):
    command = command[command.find('--'):]
    parser = argparse.ArgumentParser(description="")
    for i in args:
        if len(i) == 3:
            print(i)
            parser.add_argument(i[0], help=i[1], type=str, default=i[2])
        else:
            parser.add_argument(i[0], help=i[1], type=str)
    return vars(parser.parse_args(command.split()))

class Loader:
    def __init__(self, vars):
        self.vars = vars

    def __print__(self, msg):
        print(f'> {msg}')

    def check_on_help(self, command):
        if 'load' in command and len(command) == 4:
            pass

    def parser(self, command):
        self.check_on_help(command)
        try:

            if 'emb' in command:
                arg_list = [['--mode',
                             'режим загрузки эмбеддингов: load - предобученные, init - новая модель, min - мини-корпус векторов'],
                            ['--freeze', 'True|False - заморозить вектора эмбеддингов pytorch-слоя'],
                            ['-save_gensim', 'True|False - сохранить и вернуть gensim keyed_vectors объект']
                            ]
                args = parse_args(command, arg_list)
                vars = build_embedding_layer(**args)
                if len(vars) == 4:
                    emb_dict = {'emb_layer' : vars[0], 'word2index' : vars[1], 'index2word' : vars[2],
                                'key_vectors' : vars[3]}
                else:
                    emb_dict = {'emb_layer': vars[0], 'word2index': vars[1], 'index2word': vars[2]}
                return emb_dict
        except Exception as e:
            error(e)


class Cooker():
    def __init__(self, vars):
        self.vars = vars

    def parser(self, command):
        try:
            if 'embed' in command:
                emb_corpus_downloader()
            elif 'intents' in command:
                arg_list = [['--batch_size',
                             'batch_size', 32],
                            ['--shuffle', 'shuffle data', True],
                            ['--drop_last', 'drop last in batch', True]
                            ]
                args = parse_args(command, arg_list)
                return intents_preproc(self.vars['word2index'], self.vars['index2word'], **args)

        except Exception as e:
            error(e)


class Runner():
    def __init__(self, vars):
        self.vars = vars

    def parser(self, command):
        try:
            if 'emb' in command  and 'decrease' in command:
                arg_list = [['--w2v_path',
                             'путь до сохраненных векторов gensim'],
                            ]
                if '--' in command:
                    args = parse_args(command, arg_list)
                    decrease(args[0])
                else:
                    decrease()
        except Exception as e:
            error(e)


class TerminalExec():
    def __init__(self):
        USE_CUDA = torch.cuda.is_available()
        self.device = torch.device("cuda" if USE_CUDA else "cpu")
        self.GLOBAL_VARS = {
            'device' : self.device
        }
        self.loader = Loader(self.GLOBAL_VARS)
        self.cooker = Cooker(self.GLOBAL_VARS)
        self.runner = Runner(self.GLOBAL_VARS)
        self.main_loop()

    def update_vars(self, dict_ob=None):
        if dict_ob == None:
            return 0
        [self.GLOBAL_VARS.update({key : dict_ob[key]}) for key in dict_ob.keys()]
        info(f'{"+".join([key for key in dict_ob])} loaded')

    def __clear__(self):
        try:
            os.system('clear')
        except Exception as e:
            error(e)

    def __print__(self, msg):
        print(f'> {msg}')

    def check_vars(self):
        info('Vars: ' + ' '.join(list(self.GLOBAL_VARS.keys())))

    def help(self):
        help = [f"""{colored('load [module]', 'red')} - загрузка модулей"""]
        help.append(f"""{colored('cook [module]', 'red')} - подготовка и загрузка данных""")
        help.append(f"{colored('exit || quit', 'red')} - выход из псевдо-оболочки")
        help.append(f"{colored('run', 'red')} - запуск скрипта из scripts")
        help.append( f"""Порядок обучения модуля:
    1) cook module
    2) load module
    3) train module""")
        for i in help:
            self.__print__('* ' + i)

    def preloader(self):
        self.mode = input('> Mode (dev, inf): ')
        if self.mode != 'dev' and self.mode != 'inf': raise NotImplementedError
        if self.mode == 'dev':
            info('Loading embeddings...')
            self.parse_commands('load emb --mode min')
            self.check_vars()

    def show_var(self, command):
        for k in self.GLOBAL_VARS.keys():
            if command in k:
                self.__print__(f"""{k}: {self.GLOBAL_VARS[k]}""")

    def parse_commands(self, command):
        if 'exit' in command or 'quite' in command:
            return 1
        elif 'load' in command:
            s = self.update_vars(self.loader.parser(command))
        elif 'cook' in command:
            s = self.update_vars(self.cooker.parser(command))
        elif 'help' in command:
            self.help()
        elif 'run' in command:
            s = self.update_vars(self.runner.parser(command))

        elif 'clear' in command: self.__clear__()
        elif command == 'vars': self.check_vars()
        else:
            self.show_var(command)

    def command(self, command=None):
        if command != None:
            return command
        return input(f'> ')

    def update_modules(self):
        self.loader.vars = self.GLOBAL_VARS
        self.runner.vars = self.GLOBAL_VARS
        self.cooker.vars = self.GLOBAL_VARS

    def main_loop(self):
        self.preloader()
        while True:
            state = self.parse_commands(self.command())
            self.update_modules()
            if state == 1:
                info('Exiting...')
                break


if __name__ == "__main__":
    t = TerminalExec()