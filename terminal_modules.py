import sys
import os
import time


ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = ROOT_DIR[:ROOT_DIR.find('qa_system') + 10]
sys.path.insert(1, ROOT_DIR)
from utils.functions import info, error, warning, good_news, out

from modules.embedding_module.embed_main import build_embedding_layer
from modules.intents_detector_module.intents_data_preprocessor import main as intents_preproc
from scripts.emb_data_downloader import main as emb_corpus_downloader
from scripts.w2v_decrease import decrease
import torch

import importlib
from cmd import Cmd


class Loader:
    def __init__(self, vars):
        self.vars = vars

    def parse(self, type, args):
        try:
            if 'emb' in type:
                emb_dict = build_embedding_layer(**args)
                return emb_dict
        except Exception as e:
            error(e)

class Cooker():
    def __init__(self, vars):
        self.vars = vars

    def parse(self, type, args):
        try:
            if 'emb' in type:
                emb_corpus_downloader()
            elif 'intents' in type:
                return intents_preproc(self.vars['embedding_dock']['word2index'],
                                       self.vars['embedding_dock']['index2word'], **args)

        except Exception as e:
            error(e)

class Runner():
    def __init__(self, vars):
        self.vars = vars

    def parse(self, type, args=None):
        try:
            if 'emb_decrease' in type:
                if args != None:
                    decrease(**args)
                else:
                    decrease()
        except Exception as e:
            error(e)
