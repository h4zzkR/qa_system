import os.path
import sys
import multiprocessing
import torch
import numpy as np
from yaspin import yaspin
from tqdm import tqdm_notebook as tqdm
from sklearn.metrics import accuracy_score
from torch.utils.data import DataLoader

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = ROOT_DIR[:ROOT_DIR.find('qa_system') + 10]
sys.path.insert(1, ROOT_DIR)

from utils.functions import info, error, out


class Trainer(object):
    def __init__(self, dataset, bsize, device, epochs, emb_layer, emb_dim, save_dir,
                 save_every, print_every, clip=5, shuffle=True):
        self.dataloader = DataLoader(dataset, batch_size=bsize, shuffle=shuffle,
                                num_workers=multiprocessing.cpu_count(), drop_last=True, pin_memory=True)
        self.batch_size = bsize
        self.nepochs = epochs
        self.save_dir = save_dir
        self.save_every = save_every
        self.print_every = print_every
        self.clip = clip
        np.random.seed(0)

        self.iters = 0
        self.n_correct = 0
        self.epoch = 0

        self.device = device
        self.max_length = dataset.max_len
        self.collate_fn = dataset.collate_fn
        self.emb = emb_layer
        self.emb_dim = emb_dim

    def init_trainer(self, lr):
        pass

    def build_models(self, hidden_dim, n_layers=1, dropout=0):
        pass

    def train_cycle(self):
        pass

    def toggle_train(self):
        pass

    def train_iter(self):
        self.toggle_train()
