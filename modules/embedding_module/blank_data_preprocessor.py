import sys
import json
import os
import six
from gensim.corpora import WikiCorpus
import nltk
import logging

from yaspin import yaspin

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = ROOT_DIR[:ROOT_DIR.find('qa_system') + 10]
sys.path.insert(1, ROOT_DIR)

PRESET_PATH = os.path.join(ROOT_DIR, "data/embed.module/embed.data_urls.json")

class DataUnpacker(object):
    def __init__(self, blocklist):
        self.presets = None
        self.blocklist = blocklist
        self.raws = {}
        self.corpuses = None
        self.DIRECTORY = os.path.join(ROOT_DIR, 'data/embed.module/corpuses/')
        self._read_properties_(PRESET_PATH)
        self._init_pathes_()

    def _read_properties_(self, path):
        with open(path, 'r') as f:
            self.presets = json.load(f)

    def _init_pathes_(self):
        self.corpuses = {}
        for corpus in self.presets.keys():
            path = os.path.join(ROOT_DIR, 'data/embed.module/corpuses/')
            self.corpuses.update({corpus : path + self.presets[corpus]['name']})

    def _unzip_gz(self, path, name, path2unzip=None):
        pass

    def main(self):
        pass

class WikiPreprocessor(object):
    def __init__(self, name, inp):
        self.process(name, inp)

    def process(self, name, inp):
        pass
