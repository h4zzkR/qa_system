#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os.path
import sys
import json
import logging
import multiprocessing
import gensim
from gensim.models.word2vec import LineSentence

from yaspin import yaspin

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = ROOT_DIR[:ROOT_DIR.find('qa_system') + 10]
sys.path.insert(1, ROOT_DIR)

from utils.functions import info, error, warning, good_news
from torch.nn import Embedding
import torch
from scripts.w2v_decrease import decrease

PRESET_PATH = os.path.join(ROOT_DIR, "configs/embed_module/param_config.json")

class Embeddings(object):
    def __init__(self, mode='init'):
        self._read_properties_()
        if mode == 'init':
            #TODO PADDING INDEX
            info('Setting module for training model...')
            self.presets = self.presets['init']
            self.type = self.presets['type']
            info(f'{self.type.title()} model will be trained...')
            self.logger = logging.getLogger('Embedding_engine')
            self.corpus_path = os.path.join(ROOT_DIR, self.presets['corpus_path'])
            self.save_only_vecs = True if self.presets['save_only_vecs'] == "True" else False
            self.model_save_dir = os.path.join(ROOT_DIR, self.presets['model_save_dir'])
            self.window_size = self.presets['window']
            self.epochs = self.presets['epochs']
            self.emb_dim = self.presets['emb_dim']
            self._init_()

        elif mode == 'load':
            info('Setting module for loading pretrained vectors...')
            self.presets = self.presets['load']
            self._declarator_(self.presets['binaries_location'])
            self.pad_idx = self.presets['pad_index']
            self.binaries_location = os.path.join(ROOT_DIR, self.presets['binaries_location'])
            self._load_()
        elif mode == 'min':
            info('Setting module for loading baseline pretrained vectors...')
            self.pad_idx = self.presets['load']['pad_index']
            self.binaries_location = os.path.join(ROOT_DIR, self.presets['min']['binaries_location'])
            self._declarator_(self.presets['min']['binaries_location'])
            if not os.path.exists(self.binaries_location):
                warning('Decreased vectors not found, initializing...')
                decrease(self.presets['min']['baseline'])
            self._load_()

    def _read_properties_(self):
        with open(PRESET_PATH, 'r') as f:
            self.presets = json.load(f)


    def _declarator_(self, path):
        self.emb_dim = int(path[path.find('DIM=') + 4:path.find('.bin')])
        self.window_size = int(path[path.find('WIN=') + 4:path.find('_DIM=')])


    def _load_(self):
        with yaspin(text="  Loading vectors...", color="yellow") as spinner:
            try:
                self.wv = gensim.models.KeyedVectors.load_word2vec_format(self.binaries_location,
                                                                                        binary=True)
                spinner.ok("✅")
            except Exception as e:
                spinner.fail("💥")
                error(e)

    def word2index(self, word):
        try:
            return self.wv.vocab[word].index
        except Exception as e:
            error(e)

    def index2word(self, index):
        return self.wv.index2word[index]

    def _init_(self):
        logging.basicConfig(format='%(asctime)s : %(levelname)s : %(message)s')
        logging.root.setLevel(level=logging.INFO)
        self.logger.info("running %s" % ' '.join(sys.argv))

        if self.type == 'fasttext':
            self.model = gensim.models.FastText(LineSentence(self.corpus_path), size=self.emb_dim,
                                                window=self.window_size, min_count=self.epochs,
                                                workers=multiprocessing.cpu_count())
        elif self.type == 'w2v':
            self.model = gensim.models.Word2Vec(LineSentence(self.corpus_path), size=self.emb_dim,
                                                window=self.window_size, min_count=self.epochs,
                                                workers=multiprocessing.cpu_count())
        else:
            raise NotImplementedError
        self.wv = self.model.wv
        self._save_model_()

    def _save_model_(self):
        fname = f'{self.type.upper()}.WIN={self.window_size}_DIM={self.emb_dim}.bin'
        self.wv.save(self.model_save_dir + fname)
        if not self.save_only_vecs:
            self.model.save(f'{self.type.upper()}.WIN={self.window_size}_DIM={self.emb_dim}.model')

def build_embedding_layer(mode='load', freeze=True, save_gensim=False):
    embedding_object = Embeddings(mode=mode)
    emb_layer = Embedding.from_pretrained(torch.FloatTensor(embedding_object.wv.vectors),
                                               padding_idx=embedding_object.pad_idx, freeze=freeze)
    #bug
    word2index = {w : embedding_object.word2index(w) for w in embedding_object.wv.vocab.keys()}
    index2word = {word2index[w] : w for w in word2index.keys()}
    emb_dict = {'embedding_dock' : {'emb_layer': emb_layer, 'word2index': word2index, 'index2word': index2word,
                'key_vectors': embedding_object, 'emb_dim' : embedding_object.emb_dim}}
    if save_gensim:
        emb_dict['embedding_dock'].update({'key_vectors': embedding_object})
    return emb_dict
    #word -> index -> emb_layer

def build_load_gensim(mode='load'):
    return Embeddings(mode=mode)

if __name__ == "__main__":
    emb = Embeddings(mode='load')