import sys
import json
import os
import time
import logging
import nltk
import six

from yaspin import yaspin

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = ROOT_DIR[:ROOT_DIR.find('qa_system') + 10]

DIRECTORY = os.path.join(ROOT_DIR, 'data/embed.module/corpuses/')

sys.path.insert(1, ROOT_DIR)

from utils.functions import info, error
import multiprocessing
from .blank_data_preprocessor import DataUnpacker, WikiPreprocessor
from gensim.corpora import WikiCorpus

# cd /target/directory; bzip2 -d /path/to/achive/archive.bz2


class EmbedDataUnpacker(DataUnpacker):
    def __init__(self, blocklist=None):
        self.presets = None
        self.blocklist = blocklist
        self.raws = {}
        self.corpuses = None
        self.DIRECTORY = os.path.join(ROOT_DIR, 'data/embed.module/corpuses/')
        super().__init__(blocklist)
        self.main()

    def _unzip_gz(self, path, name, path2unzip=None):
        # multiprocessing.cpu_count()
        start_time = time.time()
        cpus = multiprocessing.cpu_count()
        with yaspin(text=f"Unzipping {name}...", color="yellow") as spinner:
            if path2unzip is None:
                os.system(f'cd {self.DIRECTORY} && pbzip2 -dk -p{cpus} {path}')
            else:
                os.system(f'cd {path2unzip} && pbzip2 -dk -p{cpus} {path}')
            spinner.ok("✅ " + "--- %s seconds ---" % (time.time() - start_time))
        # os.system(f'rm -rf {path}')

    def _process_ruwiki(self, path):
        name = self.presets['ruwiki_corpus']['name']
        self._unzip_gz(path, name)

    def _process_taiga(self, path):
        self._unzip_gz(path)

    def main(self):
        for k in self.corpuses:
            if any(k_ in key for key in self.blocklist for k_ in k.split('_')):
                pass
            elif 'ruwiki' in k:
                self._process_ruwiki(self.corpuses[k])
                self.raws.update({k : os.path.join(self.DIRECTORY, 'ruwiki_corpus.xml')})
            elif 'taiga' in k:
                print('TAIGA HERE')
                # self._process_taiga


class WikiPreprocessor(WikiPreprocessor):
    def __init__(self, name, inp):
        super().__init__(name, inp)

    def process(self, name, inp):
        out = os.path.join(DIRECTORY, f'{name}.txt')
        space = " "
        i = 0
        output = open(out, 'w')
        wiki = WikiCorpus(inp, lemmatize=False, dictionary={})
        with yaspin().white as sp:
            for text in wiki.get_texts():
                if six.PY3:
                    output.write(' '.join(text) + '\n')
                else:
                    output.write(space.join(text) + "\n")
                i = i + 1
                if (i % 1 == 0):
                    sp.text = "Saved " + str(i) + " articles"

            output.close()
            sp.text = "✅ " + "Saved " + str(i) + " articles"


class TaigaPreprocessor(object):
    def __init__(self, name, object):
        pass


class GlobalPreprocessor(object):
    def __init__(self, config):
        self.__read_config__(config)
        self.preprocess()

    def __read_config__(self, path):
        with open(path, 'r') as f:
            self.config = json.load(f)

    def preprocess(self):
        for k in self.config.keys():
            path = [k, os.path.join(ROOT_DIR, self.config[k]['name'])]
            if 'wiki' in k.lower():
                WikiPreprocessor(*path)
            elif 'taiga' in k.lower():
                TaigaPreprocessor(*path)


def main(config_path):
    GlobalPreprocessor(config_path)


if __name__ == "__main__":
    blocklist = ['taiga']
    # unpacked = EmbedDataUnpacker(blocklist)
    pathes = ["ruwiki_corpus", os.path.join(DIRECTORY, "ruwiki_corpus.xml.bz2")]
    processor = WikiPreprocessor(*pathes)
