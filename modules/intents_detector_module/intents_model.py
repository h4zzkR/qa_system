import os.path
import sys
import multiprocessing
import torch
import numpy as np
from yaspin import yaspin
from tqdm import tqdm_notebook as tqdm

import torch.nn.functional as F
import torch.nn as nn
from sklearn.metrics import accuracy_score

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = ROOT_DIR[:ROOT_DIR.find('qa_system') + 10]
sys.path.insert(1, ROOT_DIR)

from utils.functions import info, error, out


class CNNClassifier(nn.Module):

    def __init__(self, embedding_layer, embedding_dim,
                 output_size, kernel_dim=100, kernel_sizes=(3, 4, 5), dropout=0.5):
        super(CNNClassifier, self).__init__()

        self.embedding = embedding_layer
        self.convs = nn.ModuleList([nn.Conv2d(1, kernel_dim, (K, embedding_dim)) for K in kernel_sizes])

        # kernal_size = (K,D)
        self.dropout = nn.Dropout(dropout)
        self.fc = nn.Linear(len(kernel_sizes) * kernel_dim, output_size)

    def forward(self, inputs, inputs_len, inputs_mask, is_training=False):
        inputs = self.embedding(inputs).unsqueeze(1)  # (B,1,T,D)
        inputs = [F.relu(conv(inputs)).squeeze(3) for conv in self.convs]
        inputs = [F.max_pool1d(i, i.size(2)).squeeze(2) for i in inputs]  # [(N,Co), ...]*len(Ks)

        concated = torch.cat(inputs, 1)

        if is_training:
            concated = self.dropout(concated)  # (N,len(Ks)*Co)
        out = self.fc(concated)
        return F.log_softmax(out, 1)