import os.path
import sys
import multiprocessing
import torch
import numpy as np
from razdel import tokenize
from torch.utils.data import DataLoader
from torch.utils.data import Dataset

import importlib

from yaspin import yaspin

#TODO Используй автожнкодеры и потом сравнивай вектора для модели синонимичсности

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = ROOT_DIR[:ROOT_DIR.find('qa_system') + 10]
sys.path.insert(1, ROOT_DIR)

from utils.functions import info, error
from utils.tokenizer import TokenEngine


class Preprocessor():
    def __init__(self, w2i, i2w, pad_idx):
        self.intents = self.load_intents()
        self.max_len = self.__get_max_len__()
        self.max_len += round(0.3 * self.max_len)
        self.tokenizer = TokenEngine(w2i, i2w, pad_idx, self.max_len)
        self.intents = self.tokenize(self.intents)

    def __get_max_len__(self):
        ls = [self.intents[k] for k in self.intents.keys()]
        lines = []
        for l in ls:
            lines += l
        return max([len(l) for l in lines])

    def __get_intent__(self, line):
        return line[line.find(':') + 1:].replace('\n', '')

    def tknz(self, lines):
        return [[_.text for _ in list(tokenize(l))] for l in lines]

    def load_intents(self):
        path = os.path.join(ROOT_DIR, 'data/intents_detector_module/intents.txt')
        file = open(path, 'r')
        intents = {}
        elements = []
        prev_intent = '## intent:greetings'
        line = None
        while line != '______________________________________________________________________\n':
            line = file.readline()
        line = file.readline()
        for line in file.readlines():
            if '## intent:' in line:
                intents.update({self.__get_intent__(prev_intent) : elements[:-1]})
                prev_intent = line
                elements = []
            else:
                elements.append(line)
        return intents

    def tokenize(self, intents):
        for k in intents.keys():
            lines = intents[k]
            intents[k] = self.tokenizer.prepare_sentences(lines)
        return intents


class IntentsDataset(Dataset):
    def __init__(self, intents):
        self.intents = intents.intents
        self.max_len = intents.max_len
        self.intent2int = {key : number for number, key in enumerate(self.intents.keys())}
        self.big_dataset = self.init()
        info(f'Loaded {self.size()} intents.')
        self.size = len(self.big_dataset)

    def size(self):
        count = 0
        for k in self.intents.keys():
            count += len(self.intents[k])
        return count

    def n(self, array):
        return np.array(array)

    def t(self, array):
        return torch.LongTensor(array)

    def cat(self, cat_to, cat_from):
        assert len(cat_to) == len(cat_from)
        for i in range(len(cat_to)):
            cat_to[i] += cat_from[i]
        return cat_to

    def init(self):
        big_dataset = [[], [], [], []]
        for k in self.intents.keys():
            intent = [self.intent2int[k]]
            intent_replyes = self.intents[k][0]
            count = len(intent_replyes)
            intent_replyes_lens = self.intents[k][1]
            intent_replyes_masks = self.intents[k][2]
            to_cat = [[intent for _ in range(count)], intent_replyes,
                      intent_replyes_lens, intent_replyes_masks]
            big_dataset = self.cat(big_dataset, to_cat)
        return big_dataset

    def __len__(self):
        return len(self.big_dataset[0])

    def __getitem__(self, index):
        intent = self.big_dataset[0][index]
        intent_r = self.big_dataset[1][index]
        intent_rlen = [self.big_dataset[2][index]]
        intent_mask = self.big_dataset[3][index]
        return self.t(self.n(intent)), self.t(self.n(intent_r)),\
               self.t(self.n(intent_rlen)), self.t(self.n(intent_mask))


def main(w2v, i2w, idx=1):
    prep = Preprocessor(w2v, i2w, idx)
    data = IntentsDataset(prep)
    return {'intents_dataset' : data}
    #DEBUGGED!


if __name__ == "__main__":
    intents = Preprocessor()