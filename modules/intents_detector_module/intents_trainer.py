import os.path
import sys
import multiprocessing
import torch
import numpy as np
from yaspin import yaspin
from tqdm import tqdm_notebook as tqdm
from sklearn.metrics import accuracy_score

import torch.nn.functional as F
import torch.nn as nn

ROOT_DIR = os.path.dirname(os.path.abspath(__file__))
ROOT_DIR = ROOT_DIR[:ROOT_DIR.find('qa_system') + 10]
sys.path.insert(1, ROOT_DIR)

from utils.functions import info, error, out

from modules.blank_module.blank_trainer import Trainer
from modules.intents_detector_module.intents_model import Classifier


class IntentsTrainer(Trainer):
    def __init__(self, dataset, bsize, device, epochs, emb_layer, emb_dim,
                 save_every, print_every, clip=5, shuffle=True,
                 save_dir=os.path.join(ROOT_DIR, 'models/intent_model')):
        super(IntentsTrainer, self).__init__(dataset, bsize, device, epochs, emb_layer, emb_dim, save_dir,
                 save_every, print_every, clip=5, shuffle=True)

    def build_models(self, hidden_dim, n_layers=1, dropout=0):
        self.model = Classifier(hidden_dim, self.emb_dim, n_layers, dropout).to(self.device)

    def toggle_train(self):
        self.model.train()

    def zero_grad(self):
        self.model_opt.zero_grad()

    def optim_step(self):
        self.model_opt.step()

    def init_trainer(self, lr):
        self.model.train()
        self.model_opt = torch.optim.Adam(self.model.parameters(), lr=lr)
        self.loss_fn = nn.BCELoss()

    def train_iter(self, iteration, epoch, reply, reply_len, reply_mask, intent):
        self.toggle_train()
        self.zero_grad()
        reply_len = reply_len.to(self.device).squeeze()
        reply = reply.to(self.device).squeeze()
        reply_mask = reply_mask.to(self.device) #type(torch.FloatTensor)
        intent = intent.to(self.device)
        loss = 0
        intent_class = self.model(reply, reply_len, reply_mask)
        loss = self.loss_fn(intent_class, intent)
        loss.backward()

        _ = torch.nn.utils.clip_grad_norm_(self.model.parameters(), self.clip)
        self.optim_step()
        return loss, intent_class, intent

    def save_models(self):
        if not os.path.exists(self.save_dir + 'intent_model.pth'):
            torch.save(self.model, self.save_dir + 'intent_model.pth')
            # torch.save(self.predictor, self.save_dir + 'predictor_model.pth')

        torch.save({
            'model': self.model,
            'model_state_dict': self.model.state_dict(),
            'optimizer_model_state_dict': self.model_opt.state_dict(),
            'batch_size': self.batch_size,
            'loss_fn': self.loss_fn,
            'epoch': self.epoch,
        }, self.save_dir + f'{self.epoch}_checkpoint.pth')
        print('Checkpoint saved')

    def load_models(self, filepath, train=True):
        checkpoint = torch.load(filepath)

        self.model = torch.load(self.save_dir + 'intent_model.pth')

        self.model.load_state_dict(checkpoint['model_state_dict'])
        self.epoch = checkpoint['epoch']

        if train:
            self.model_opt.load_state_dict(checkpoint['optimizer_model_state_dict'])

            self.loss_fn = checkpoint['loss_fn']

            for parameter in self.model.parameters():
                parameter.requires_grad = True

            self.toggle_train()
        else:
            for parameter in self.model.parameters():
                parameter.requires_grad = False
            self.model.eval()
        info('All models loaded')

    def accuracy(self, batch_pred, batch_true):
        for i in range(self.batch_size):
            if 0.55 < batch_pred[i] <= 1 and batch_true[i] == 1:
                self.n_correct += 1
            self.iters += 1

    def train_cycle(self):
        print('$ Started training SynonymyDetector')
        for epoch in range(self.epoch, self.nepochs):
            for iteration, batch in enumerate(tqdm(self.dataloader)):
                batch = self.collate_fn(batch)
                batch.update({'iteration': iteration, 'epoch': epoch})
                loss, out, rel = self.train_iter(**batch)
                self.epoch = epoch + 1
                self.accuracy(out, rel)
            self.save_models()
            print(f'Final epoch loss is: {loss.item()}')
            print(f'Epoch accuracy is {self.n_correct / self.iters * 100}')